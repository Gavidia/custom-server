const CustomSocket = require('./custom_socket');
const { EventEmitter } = require('events');
const { Server } = require('net');
const Client = require('./client');

class CustomServer extends EventEmitter{

  constructor(connListener){

    super();

    this._server = new Server();
    this._server.on('connection', this._onConnection.bind(this));
    this._server.on('error', () => this.emit('error', err));
    this._server.on('close', () => this.emit('close'));
    this._server.on('listening', () => this.emit('listening'));

    if (connListener) this.on('connection', connListener);

  };

  listen(opts, callback) {
    this._server.listen(opts, callback);
    return this;
  };

  _onConnection(socket) {

    console.log("nueva conexión");
    let customSocket = new CustomSocket(socket);
    new Client(this._server, customSocket);
  }

  get listening() {
    return this._server.listening;
  }

  getConnections(cb) {
    this._server.getConnections(cb);
  }
};

module.exports = CustomServer;
