
class Client{

  constructor(server, conn){
    
    this.server = server;
    this.conn = conn;
    this.setup();

  }

  setup(){

    this.ondata = this.ondata.bind(this);  
    this.conn.on('data', this.ondata);
  }

  ondata(data){
    console.log("data?", data);
  }


  
}
    
module.exports = Client;