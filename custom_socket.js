const { Duplex } = require('stream');


class CustomSocket extends Duplex {

  constructor(socket){
    super({ objectMode: true });
    this._readingPaused = false;
    this._socket;
    if(socket) this._wrapSocket(socket);
  }

  
  _wrapSocket(socket) {

    console.log("wrap")

    this._socket = socket;

    // these are simply passed through
    this._socket.on('close', hadError => this.emit('close', hadError));
    this._socket.on('connect', () => this.emit('connect'));
    this._socket.on('drain', () => this.emit('drain'));
    this._socket.on('end', () => this.emit('end'));
    this._socket.on('error', err => this.emit('error', err));
    this._socket.on('lookup', (err, address, family, host) => this.emit('lookup', err, address, family, host)); // prettier-ignore
    this._socket.on('ready', () => this.emit('ready'));
    this._socket.on('timeout', () => this.emit('timeout'));
    this._socket.on('readable', this._onReadable.bind(this));

  };

  _onReadable(){
      
    while(!this._readingPaused){

      let data = this._socket.read();

      if (!data) return;

      let json;
      try {
        json = JSON.parse(data);
      } catch (error) {
        console.log("error en la conversión a JSON");
        this._socket.destroy();
        return;
      }

      if(!json.hasOwnProperty("EventName")){
        console.log("Evento es requerido");
        this._socket.destroy();
        return;
      }

      let isPushed = this.push(json);


      if(!isPushed) this._readingPaused = true; 
    }

    
  }
  
  _read() {
    this._readingPaused = false;
  }

}

module.exports = CustomSocket;
