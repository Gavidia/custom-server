const { Socket } = require('net');
const { Duplex } = require('stream');

class CustomSocket extends Duplex {

  constructor(socket) {

    super({ objectMode: false });

    this._readingPaused = false;

    this._socket;

    if (socket) this._wrapSocket(socket);
  };

  // connect({ host, port }) {
  //   console.log("me conecté")
  //   this._wrapSocket(new Socket());
  //   this._socket.connect({ host, port });
  //   return this;
  // };

  _wrapSocket(socket) {

    console.log("wrap")

    this._socket = socket;

    // these are simply passed through
    this._socket.on('close', hadError => this.emit('close', hadError));
    // this._socket.on('connect', () => this.emit('connect'));
    this._socket.on('connect', () => this._onConnection.bind(this));
    this._socket.on('drain', () => this.emit('drain'));
    this._socket.on('end', () => this.emit('end'));
    this._socket.on('error', err => this.emit('error', err));
    this._socket.on('lookup', (err, address, family, host) => this.emit('lookup', err, address, family, host)); // prettier-ignore
    this._socket.on('ready', () => this.emit('ready'));
    this._socket.on('timeout', () => this.emit('timeout'));
    this._socket.on('readable', this._onReadable.bind(this));

  };

  _onReadable() {

    while(!this._readingPaused) {

      let data = this._socket.read();
      
      let json;
      try {
        json = JSON.parse(data);
      } catch (ex) {
        this.socket.destroy(ex);
        return;
      }

      console.log(json);
      this._readingPaused = true;

    }
    
  }

  // _read() {
  //   console.log("read");

  //   this._readingPaused = false;
  //   setImmediate(this._onReadable.bind(this));
  // }

  // _write(obj, encoding, cb) {
  //   console.log("write");
  //   let json = JSON.stringify(obj);
  //   console.log("json", json)
  //   console.log("json", json);
  //   let jsonBytes = Buffer.byteLength(json);
  //   let buffer = Buffer.alloc(4 + jsonBytes);
  //   buffer.writeUInt32BE(jsonBytes);
  //   console.log("buffer", buffer.readUInt32BE())
  //   buffer.write(json, 4);
  //   this._socket.write(buffer, cb);
  // }

  // _final(cb) {
  //   console.log("final");

  //   this._socket.end(cb);
  // }
}

module.exports = CustomSocket;
